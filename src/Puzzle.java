import java.util.ArrayList;
import java.util.List;

public class Puzzle {

   /**
    * Solve the word puzzle.
    *
    * @param args three words (addend1 addend2 sum)
    */

   public static void main(String[] args) {
      Puzzle puzzle = new Puzzle(args);
      puzzle.solver();
   }

   private final String[] args;

   public Puzzle(String[] args) {
      if (args.length != 3) {
         throw new RuntimeException("Please provide exactly 3 arguments (words)!");
      }
      this.args = args;
   }

   private List<Character> getUniqueLetters() {
      List<Character> uniqueLetters = new ArrayList<>();
      for (String arg : args) {
         for (char c : arg.toCharArray()) {
            if (!uniqueLetters.contains(c)) {
               if (uniqueLetters.size() >= 10)
                  throw new RuntimeException("Words '" + args[0] + "', '" + args[1] + "' and '" + args[2] + "' combined have more than 10 unique letters!");
               uniqueLetters.add(c);
            }
         }
      }
      return uniqueLetters;
   }

   private PuzzleItem[] getPuzzleItems() {
      PuzzleItem[] res = new PuzzleItem[getUniqueLetters().size()];
      int i = 0;
      for (Character uniqueLetter : getUniqueLetters()) {
         res[i] = new PuzzleItem(uniqueLetter);
         i++;
      }
      return res;
   }

   private PuzzleItem getPuzzleItemById(PuzzleItem[] board, char id) {
      for (PuzzleItem puzzleItem : board) {
         if (puzzleItem.name == id) {
            return puzzleItem;
         }
      }
      return null;
   }

   private boolean isValidSolution(PuzzleItem[] board) {
      long[] argValues = new long[3];

      int i = 0;
      for (String arg : args) {
         char[] charArr = arg.toCharArray();
         long multiplier = 1;
         for (int j = charArr.length - 1; j >= 0; j--) {
            int charValue = getPuzzleItemById(board, charArr[j]).getValue();
            if (j == 0 && charValue == 0) {
               return false;
            }
            argValues[i] = argValues[i] + charValue * multiplier;
            multiplier *= 10;
         }
         i++;
      }

      return argValues[0] + argValues[1] == argValues[2];
   }

   private boolean isNumberTaken(PuzzleItem[] board, int number) {
      for (PuzzleItem puzzleItem : board) {
         if (puzzleItem.boardPlace == number) {
            return true;
         }
      }
      return false;
   }

   private void printSolution(PuzzleItem[] board, long solutionNr) {
      StringBuilder boardString = new StringBuilder("  -> Solution #" + solutionNr + " [ ");
      for (PuzzleItem puzzleItem : board) {
         boardString.append(puzzleItem).append(" ");
      }
      boardString.append("]");
      System.out.println(boardString);
   }

   // Solver source: https://enos.itcollege.ee/~jpoial/algoritmid/tehnikad.html
   private void solver() {
      PuzzleItem[] board = getPuzzleItems();

      int size = 10;
      long numOfSolutions = 0;
      int n = 0;
      do {
         PuzzleItem boardItem = board[n];

         if (boardItem.boardPlace < size) {
            int nr = boardItem.boardPlace + 1;
            if (!isNumberTaken(board, nr)) {
               boardItem.boardPlace++;
               if (n < board.length - 1) {
                  n++;
               } else {
                  if (isValidSolution(board)) {
                     numOfSolutions++;

                     if (numOfSolutions == 1) {
                        System.out.println("First found solution for '" + args[0] + " + " + args[1] + " = " + args[2] + "' is:");
                        printSolution(board, numOfSolutions);
                     }
                  }
               }
            } else {
               boardItem.boardPlace++;
            }
         } else {
            boardItem.boardPlace = 0;
            n--;
         }
      } while (n >= 0);

      if (numOfSolutions > 0) {
         System.out.println("Total solutions found for '" + args[0] + " + " + args[1] + " = " + args[2] + "': " + numOfSolutions + " solutions");
      } else {
         System.out.println("No solutions found for '" + args[0] + " + " + args[1] + " = " + args[2] + "'!");
      }
      System.out.println();
   }

   private class PuzzleItem {
      private final char name;
      public int boardPlace = 0;

      public PuzzleItem(char name) {
         this.name = name;
      }

      public int getValue() {
         return boardPlace - 1;
      }

      @Override
      public String toString() {
         return "{" + name + "=" + getValue() + "}";
      }
   }
}